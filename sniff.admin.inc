<?php

function sniff_admin_settings() {

  $form = array();

  $form['general'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Site Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );  
  
  $siteid = variable_get('sniff_siteid', NULL);
  if(!$siteid) {
      $siteid=strtolower(variable_get('site_name', NULL));
      $siteid=preg_replace("/\W/", "-", $siteid);
  }
  
  $form['general']['sniff_siteid'] = array(
  	'#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('sniff_siteid', $siteid),
    '#description' => t('Site ID to be used in logging. Identifies this site for remote logging.')
  );  
  
  $form['form_logging'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Form Logging'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );  
  
  
  $form['form_logging']['sniff_form_logging'] = array(
  	'#type' => 'checkbox',
    '#title' => t('Enable form submission logging'),
    '#default_value' => variable_get('sniff_form_logging', NULL),
    '#description' => t('Enables logging of all forum submission logging.')
  );   

  $form['form_logging']['sniff_log_paths'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Log '),
    '#default_value' => variable_get('sniff_log_paths', 'admin/*'),
  );  
  
  $form['variable_logging'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Variable Logging'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );  
  
  
  $form['variable_logging']['sniff_variable_logging'] = array(
  	'#type' => 'checkbox',
    '#title' => t('Enable system variable logging'),
    '#default_value' => variable_get('sniff_variable_logging', NULL),
    '#description' => t('Enables logging of all system variable changes.')
  );  

  $form['variable_logging']['sniff_ignore_variables'] = array(
  	'#type' => 'textarea',
    '#title' => t('Ignore variables'),
    '#default_value' => variable_get('sniff_ignore_variables', NULL),
    '#description' => t('List of variables to ignore, one per line. These variables wil not be logged.')
  );  
  
  $form['variable_logging']['sniff_hide_ignored_variables'] = array(
  	'#type' => 'checkbox',
    '#title' => t('Hide ignored variables'),
    '#default_value' => variable_get('sniff_hide_ignored_variables', NULL),
    '#description' => t('Hides already logged variables that are in the ignore list.')
  );   
  
  
  $form['inline_history'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Inline History'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );  
    
  $form['inline_history']['sniff_inline_history'] = array(
  	'#type' => 'checkbox',
    '#title' => t('Show inline history.'),
    '#default_value' => variable_get('sniff_inline_history', NULL),
    '#description' => t('Shows a history fieldset on forms where content was altered.')
  );    
  
  $form['inline_history']['sniff_inline_history_items'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of history records'),
    '#default_value' => variable_get('sniff_inline_history_items', 25),
    '#description' => t('How many records to show in a block.')
  );
  
  $form['sniff_xmlrpc'] = array(
   '#type' => 'fieldset',
    '#title' => t('Remote logging'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['sniff_xmlrpc']['sniff_xmlrpc_remote'] = array(
   '#type' => 'checkbox',
    '#title' => t('Enable remote logging.'),
    '#default_value' => variable_get('sniff_xmlrpc_remote', NULL),
  );

  $form['sniff_xmlrpc']['sniff_xmlrpc_remote_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Url of remote server'),
    '#default_value' => variable_get('sniff_xmlrpc_remote_url', 0),
    '#description' => t('For example: http://www.example.com')
  );
  
  /*
  $form['remote_logging'] = array(
  	'#type' => 'fieldset',
    '#title' => t('Publishing Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
 */
  
  return system_settings_form($form);
  
}