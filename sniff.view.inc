<?php
function sniff_view() {
  
  if(!arg(2)) {
    drupal_set_message(t('No log is available for the selected record.'), 'warning', FALSE);
    drupal_goto('admin/sniff/logs');    
  }

  $record = db_fetch_object(db_query("SELECT * FROM sniff_logs WHERE sid=%s", arg(2)));
  
  
  if($record->type=='variable') {
    drupal_set_title(t('Changes to the system variable %variable', array('%variable'=>$record->variable)));
  }
  elseif($record->type=='form') {
    drupal_set_title(t('Changes to the form %variable', array('%variable'=>$record->variable)));
  }
  
  $old_data = var_export(unserialize($record->old_value), TRUE);
  $new_data = var_export(unserialize($record->new_value), TRUE);
  
  $form['header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE    
  );
  
  if($record->type=='variable') {
    $form['header']['variable'] = array(
      '#type' => 'item',
      '#title' => t('Variable'),
      '#value' => $record->variable,
      '#prefix' => '<div class="from container-inline">',
      '#suffix' => '</div>'
    );
  }
  
  if($record->type=='form') {
    $form['header']['form'] = array(
      '#type' => 'item',
      '#title' => t('Form'),
      '#value' => $record->variable,
      '#prefix' => '<div class="from container-inline">',
      '#suffix' => '</div>'
    );
  }  
  
  $form['header']['user'] = array(
    '#type' => 'item',
    '#title' => t('User'),
    '#value' => $record->username,
    '#prefix' => '<div class="from container-inline">',
    '#suffix' => '</div>'
  );
  
  $form['header']['date'] = array(
    '#type' => 'item',
    '#title' => t('Date'),
    '#value' => _sniff_format_date($record->timestamp),
    '#prefix' => '<div class="from container-inline">',
    '#suffix' => '</div>'
  );
  
  $form['header']['url'] = array(
    '#type' => 'item',
    '#title' => t('URL'),
    '#value' => l($record->url, $record->url),
    '#prefix' => '<div class="from container-inline">',
    '#suffix' => '</div>'
  );
    
  
  
  $form['old_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Old Value'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
  $form['old_data']['data'] = array(
    '#type' => 'item',
    '#value' => $old_data,
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
  );
  
  $form['new_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('New Value'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
  $form['new_data']['data'] = array(
    '#type' => 'item',
    '#value' => $new_data,
    '#prefix' => '<pre>',
    '#suffix' => '</pre>',
  );  
  return $form;
}

function theme_sniff_view($form) {
  $output .= drupal_render($form);
  
  return $output;
}
?>