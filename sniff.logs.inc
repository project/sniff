<?php
function sniff_logs() {
  
  drupal_set_title(t('Sniff Logs'));

  
  

    
  

  
  
  $form['header'] = array('#type' => 'value', 
    '#value' => array(
      theme('table_select_header_cell'),
      array('data' => t('Username'), 'field' => 'username'),
      array('data' => t('SiteID'), 'field' => 'siteid'),
      array('data' => t('Timestamp'), 'field' => 'timestamp', 'sort' => 'desc'),
      array('data' => t('Type'), 'field' => 'type'),
      array('data' => t('Variable'), 'field' => 'variable'),      
      array('data' => t('URL'), 'field' => 'url'),
            
    )
  );  
  
  $header = array(
    array('data' => t('Username'), 'field' => "username"),
    array('data' => t('SiteID'), 'field' => 'siteid'),
    array('data' => t('Date'), 'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Type'), 'field' => 'type'),
    array('data' => t('Variable'), 'field' => 'variable'),
    array('data' => t('URL'), 'field' => 'url'),             
  );
  
  $tablesort = tablesort_sql($header);
  $sql = "SELECT * FROM {sniff_logs} ".$tablesort;
  $result = pager_query($sql, 50);
  
  while($header = db_fetch_object($result)) {
    // if the variable was already logged but is in the ignore list, do not show it
    if($header->type=='variable' && SNIFF_HIDE_IGNORED_VARIABLES && _sniff_is_variable_ignored($header->variable)) continue;
    
    $form['sid'][$header->sid] = array('#value' => $header->sid);    
    $form['username'][$header->sid] = array('#value' => l( $header->username, 'user/'.$header->uid));
    $form['siteid'][$header->sid] = array('#value' => $header->siteid);
    $form['timestamp'][$header->sid] = array('#value' => _sniff_format_date($header->timestamp));
    $form['type'][$header->sid] = array('#value' => $header->type);
    $form['variable'][$header->sid] = array('#value' => l($header->variable, 'sniff/view/'.$header->sid));
    $form['url'][$header->sid] = array('#value' => l($header->url, 'sniff/view/'.$header->sid));
  }
  
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  
  return $form;
  
}

function theme_sniff_logs($form) {

  //drupal_set_message('theme_sniff_logs');
  
  foreach (element_children($form['sid']) as $key) {
    $rows[] = array(
      array('data'=>drupal_render($form['sid'][$key])),
      array('data'=>drupal_render($form['username'][$key])),
      array('data'=>drupal_render($form['siteid'][$key])),
      array('data'=>drupal_render($form['timestamp'][$key])),
      array('data'=>drupal_render($form['type'][$key])),
      array('data'=>drupal_render($form['variable'][$key])),
      array('data'=>drupal_render($form['url'][$key]))
            
    );  
  }
  
  
  $output .= theme('table', $form['header']['#value'], $rows);
  $output .= drupal_render($form);
  
  return $output;
}



?>