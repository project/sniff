$(document).ready(function() {
  $('form a.undo, form a.redo').click(function() {
    var form = $(this).parents("form");
    variable = form.find("input[name='form_id']").attr('value');
    $.ajax({
      type: "POST",
      url: Drupal.settings.base_path+"sniff/get_form_data",
      dataType: "json",
      data: "url="+Drupal.settings.url+"&type=form&variable="+variable+"&direction="+$(this).attr('class')+"&timestamp="+form.find("input[name='timestamp']").attr('value'),
      success: function(msg){
        if (msg['error']) {
          form.find("input[name='timestamp']").before("<span class=\"error\">"+msg['error']+"</span>");
          return false;
        }
        for(var property in msg) {
          var el = form.find("[name='"+property+"']");
          switch ( el.attr('tagName')) {
            case 'INPUT':
              switch (el.attr('type')) {
                case 'img':
                case 'radio':
                case 'file':
                break;
                case 'checkbox':
                  if (msg[property]) {
                    el.attr('checked', 'checked');
                  }
                break;
                default:
                  el.attr('value', msg[property]);  
                break;
              }
            break;
            case 'SELECT':
              el.children().each(function(index, item) {
                if ( msg[property] == $(this).attr('value') ) {
                  $(this).attr('selected', 'selected');
                }
              });
            break;
            case 'TEXTAREA':
              el.text(msg[property]);
            break;
          }
        }
      }
    });
    return false;
  });
});